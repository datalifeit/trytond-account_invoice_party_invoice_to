# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from trytond.tests.test_tryton import ModuleTestCase, with_transaction


class TestCase(ModuleTestCase):
    """Test module"""
    module = 'account_invoice_party_invoice_to'

    @with_transaction()
    def test_party(self):
        'Test configuration accounts are used as fallback on party'
        pool = Pool()
        Party = pool.get('party.party')

        party1 = Party(name='Party to pay')
        party1.save()
        self.assertIsNone(party1.customer_to_invoice)
        self.assertIsNone(party1.supplier_to_invoice)

        party2 = Party(name='Party to receive')
        party2.save()

        party = Party(name='Party')
        party.customer_to_invoice = party1
        party.supplier_to_invoice = party2
        party.save()
        self.assertEqual(party.customer_to_invoice, party1)
        self.assertEqual(party.supplier_to_invoice, party2)


del ModuleTestCase
