# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import CompanyValueMixin


parties_names = ['customer_to_invoice', 'supplier_to_invoice']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    customer_to_invoice = fields.MultiValue(
            fields.Many2One('party.party', 'Customer to invoice'))
    supplier_to_invoice = fields.MultiValue(
            fields.Many2One('party.party', 'Supplier to invoice'))
    invoice_to_parties = fields.One2Many('party.party.invoice_to',
        'party', 'Invoice to parties')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in parties_names:
            return pool.get('party.party.invoice_to')
        return super(Party, cls).multivalue_model(field)

    def multivalue_record(self, field, **pattern):
        Value = self.multivalue_model(field)
        if Value.__name__ == 'party.party.invoice_to':
            pattern = pattern.copy()
            pattern['party'] = self
            return Value(**pattern)
        return super(Party, self).multivalue_record(field, **pattern)


class PartyInvoiceTo(ModelSQL, CompanyValueMixin):
    "Party Invoice to"
    __name__ = 'party.party.invoice_to'

    party = fields.Many2One('party.party', 'Party', ondelete='CASCADE',
        select=True)
    customer_to_invoice = fields.Many2One('party.party', 'Customer to invoice')
    supplier_to_invoice = fields.Many2One('party.party', 'Supplier to invoice')

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        exist = table.table_exist(cls._table)

        super(PartyInvoiceTo, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.extend(parties_names)
        value_names.extend(parties_names)
        fields.append('company')
        migrate_property('party.party',
            field_names, cls, value_names, parent='party', fields=fields)
